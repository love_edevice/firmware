#include <pic.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#define uint8 unsigned char
#define uint16 unsigned int

__CONFIG(WDTDIS & LVPDIS & HS & PWRTDIS & BORDIS);
//WDTDIS:disable watchdog timer
//LVPDIS:low voltage programming disabled
//HS:high speed crystal/resonator
//PWRTDIS:disable power up timer
//BORDIS:disable brown out reset

#define		DI		RA0
#define		WR		RA1
#define		CS		RA2


const char * Menu0 = "   Loading...   ";
const char * Menu1 = " Satellite Name ";
const char * Menu2 = " Receiver Status";
const char * Menu3 = "          Ocean ";
const char * Menu4 = "            Idle";
const char * Menu5 = "    Receiving...";

bank1 volatile uint8 CommBuf[64];


void Delay140us(uint8 delay);
void DelayUS(uint8 delay);				
void DelayMS(uint16 delay);				
void Read_LCD_Busy(void);				
void LCD1602_Init(void);				
void Write_LCD_Mult_Byte(uint8 addr,const char* pointer,uint8 index,uint8 num);

void Write_LCD_Comd8(uint8 command);
void Write_LCD_Comd4(uint8 command);
void Write_LCD_Data4(uint8 data);

void UpdateLED(void);

void ShowMenu1(void);
void ShowMenu2(void);
//---------------------------- Delay (delay * 140) microseconds.
void Delay140us(uint8 delay)
{
	uint8 k;
   	for(;delay>0;delay--)
       	for(k=0;k<115;k++);	
}


void DelayUS(uint8 delay)
{
	while(--delay);
}



void DelayMS(uint16 delay)
{
    uint16 i;
    for(;delay>0;delay--)
        for(i=0;i<453;i++);
}


void Read_LCD_Busy(void)
{
    uint8 temp;
	while(1)
	{
		TRISB = 0xFF;			
	    CS = 0;					
	    DI = 0;					
	    WR = 1;					
	    CS = 1;					
		NOP();
	    temp = PORTB;			
	    CS = 0;					
		TRISB = 0xD0;			
		if((temp & 0x1) != 0x1)
			break;
	}
}


//8bit mode
void Write_LCD_Comd8(uint8 command)
{
	uint8 tmp, tmp2;
	tmp2 = 0;
	tmp = command & 0x80;
	tmp = tmp >> 7;
	tmp2 = tmp2 | tmp;
	tmp = command & 0x40;
	tmp = tmp >> 5;
	tmp2 = tmp2 | tmp;
	tmp = command & 0x20;
	tmp = tmp >> 3;
	tmp2 = tmp2 | tmp;
	tmp = command & 0x10;
	tmp = tmp >> 1;
	tmp2 = tmp2 | tmp;
	CS = 0;
    DI = 0;
    WR = 0;					//Write operation
	PORTB = tmp2 | 0x20;
	CS = 1;
	NOP();
	DelayMS(1);
	CS = 0;
}

//4bit mode
void Write_LCD_Comd4(uint8 command)
{
	uint8 tmp, tmp2, tmp3;
	tmp2 = 0; tmp3 = 0;
	tmp = command & 0x80;
	tmp = tmp >> 7;
	tmp2 = tmp2 | tmp;
	tmp = command & 0x40;
	tmp = tmp >> 5;
	tmp2 = tmp2 | tmp;
	tmp = command & 0x20;
	tmp = tmp >> 3;
	tmp2 = tmp2 | tmp;
	tmp = command & 0x10;
	tmp = tmp >> 1;
	tmp2 = tmp2 | tmp;
	tmp = command & 0x8;
	tmp = tmp >> 3;
	tmp3 = tmp3 | tmp;
	tmp = command & 0x4;
	tmp = tmp >> 1;
	tmp3 = tmp3 | tmp;
	tmp = command & 0x2;
	tmp = tmp << 1;
	tmp3 = tmp3 | tmp;
	tmp = command & 0x1;
	tmp = tmp << 3;
	tmp3 = tmp3 | tmp;
	//Read_LCD_Busy();
    CS = 0;
    DI = 0;
    WR = 0;					//Write operation
	PORTB = tmp2 | 0x20;
    CS = 1;
	NOP();
	DelayMS(1);
    CS = 0;
	PORTB = tmp3 | 0x20;
    CS = 1;
	NOP();
	DelayMS(1);
	CS = 0;
}


void Write_LCD_Data4(uint8 data)
{
	uint8 tmp, tmp2, tmp3;
	tmp2 = 0; tmp3 = 0;
	tmp = data & 0x80;
	tmp = tmp >> 7;
	tmp2 = tmp2 | tmp;
	tmp = data & 0x40;
	tmp = tmp >> 5;
	tmp2 = tmp2 | tmp;
	tmp = data & 0x20;
	tmp = tmp >> 3;
	tmp2 = tmp2 | tmp;
	tmp = data & 0x10;
	tmp = tmp >> 1;
	tmp2 = tmp2 | tmp;
	tmp = data & 0x8;
	tmp = tmp >> 3;
	tmp3 = tmp3 | tmp;
	tmp = data & 0x4;
	tmp = tmp >> 1;
	tmp3 = tmp3 | tmp;
	tmp = data & 0x2;
	tmp = tmp << 1;
	tmp3 = tmp3 | tmp;
	tmp = data & 0x1;
	tmp = tmp << 3;
	tmp3 = tmp3 | tmp;
	//Read_LCD_Busy();
    CS = 0;
    DI = 1;
    WR = 0;					//Write operation
	PORTB = tmp2 | 0x20;
    CS = 1;
	NOP();
	DelayMS(1);
    CS = 0;
	PORTB = tmp3 | 0x20;
    CS = 1;
	NOP();
	DelayMS(1);
	CS = 0;
}


void LCD1602_Init(void)
{
	DelayMS(15);			//wait 15ms
	Write_LCD_Comd8(0x30);	//configure LCD as 8-bit mode, 2 line mode (not use busy flag)
	DelayMS(5);				//wait 5ms
	Write_LCD_Comd8(0x30);	//configure LCD as 8-bit mode, 2 line mode (not use busy flag)
	DelayMS(5);				//wait 5ms
	Write_LCD_Comd8(0x30);	//configure LCD as 8-bit mode, 2 line mode (not use busy flag)
	DelayMS(5);				//wait some milliseconds
	Write_LCD_Comd8(0x20);	//configure LCD as 4-bit mode
	DelayMS(5);				//wait some milliseconds
	Write_LCD_Comd4(0x28);	//Function set
	DelayMS(5);				//wait some milliseconds
	Write_LCD_Comd4(0x08);	//Display off
	DelayMS(5);				//wait some milliseconds
	Write_LCD_Comd4(0x01);	//clear LCD
	DelayMS(5);				//wait some milliseconds
	Write_LCD_Comd4(0x06);	//Entry mode set
	DelayMS(5);				//wait some milliseconds
	Write_LCD_Comd4(0x48);	//Create custome character at CGRAM address = 0x01
	DelayMS(5);				//wait some milliseconds
/*	Write_LCD_Data4(0x04);	//Write custom character(underlined +) to CGRAM
	DelayMS(5);				//wait some milliseconds
	Write_LCD_Data4(0x04);
	DelayMS(5);				//wait some milliseconds
	Write_LCD_Data4(0x1F);
	DelayMS(5);				//wait some milliseconds
	Write_LCD_Data4(0x04);
	DelayMS(5);				//wait some milliseconds
	Write_LCD_Data4(0x04);
	DelayMS(5);				//wait some milliseconds
	Write_LCD_Data4(0x00);
	DelayMS(5);				//wait some milliseconds
	Write_LCD_Data4(0x1F);
	DelayMS(5);				//wait some milliseconds
	Write_LCD_Data4(0x00);
	DelayMS(5);				//wait some milliseconds      */
	Write_LCD_Comd4(0x0C);	//display on
	DelayMS(5);				//wait some milliseconds
}

void Write_LCD_Mult_Byte(uint8 addr,const char * pointer,uint8 index,uint8 num)
{
    uint8 i;
	const char *cc = pointer;
	//Read_LCD_Busy();
	DelayMS(5);
    Write_LCD_Comd4(addr);					//写地址
    for(i=0;i<num;i++)                   	//循环num次
    {
        Write_LCD_Data4(cc[index+i]);   //写数据
		DelayMS(1);
    }
}

void ShowMenu1(void)
{
	Write_LCD_Mult_Byte(0x80,Menu1,0,16);
	Write_LCD_Mult_Byte(0xC0,Menu3,0,16);
}

void ShowMenu2(void)
{
	Write_LCD_Mult_Byte(0x80,Menu2,0,16);
	Write_LCD_Mult_Byte(0xC0,Menu5,0,16);
}

void UpdateLED(void)
{
	uint8 temp;
	temp = CommBuf[49];

	RE1 = 0;	RE0 = 0;
	RE2 = temp >> 7;	RE1 = 1;	RE1 = 0;
	RE2 = temp >> 6;	RE1 = 1;	RE1 = 0;
	RE2 = temp >> 5;	RE1 = 1;	RE1 = 0;
	RE2 = temp >> 4;	RE1 = 1;	RE1 = 0;
	RE2 = temp >> 3;	RE1 = 1;	RE1 = 0;
	RE2 = temp >> 2;	RE1 = 1;	RE1 = 0;
	RE2 = temp >> 1;	RE1 = 1;	RE1 = 0;
	RE2 = temp;			RE1 = 1;	RE1 = 0;

	RE0 = 1;
}


void main()
{	
	uint8 i, j, k;
	// Starting Beep
	TRISC0 = 0;
	RC0 = 1; DelayMS(100); RC0 = 0;
	// Initialize Communication Buffer
	for ( i = 0 ; i < 64 ; i++)
		CommBuf[i] = 0;
	CommBuf[0] = 0x33; //PIC ID(Blackfin identfy PIC with this ID among peripheral devices)
	CommBuf[1] = 0x00; //Deep Level of current Menu is 0.
	CommBuf[2] = 0x01; //Current menu is 1st Menu.
	CommBuf[3] = 0x01; //Current demodulation method is QPSK. 2: BPSK, 3: OQPSK, 4: OFDM
	CommBuf[10] = 0; //Cursor Position
	CommBuf[11] = 0; // Central Freq Current Cursor Position
	CommBuf[12] = 0; // Baud Rate Current Cursor Position
	CommBuf[13] = 0; // IP Address Current Cursor Position

	ADCON1 = 0x87;			
//	SSPCON = 0x34; SSPSTAT = 0x00;
	TRISA = 0x38;

	TRISB = 0xD0;	
	LCD1602_Init();
	RB5 = 1;
	Write_LCD_Mult_Byte(0x80,Menu0,0,16);

	//CommBuf[49] 7:Wisangchuzeok 6:bansongpa 5:susin 4:wisongsinho 3:zaryozeonsong 2:chegyeoyu 1:gyongbo 0:
	TRISE = 0x0;
	CommBuf[49] = 0xFF; UpdateLED(); DelayMS(300);
	CommBuf[49] = 0x0; UpdateLED(); DelayMS(200);
	CommBuf[49] = 0x10; UpdateLED(); DelayMS(200);
	CommBuf[49] = 0x20; UpdateLED(); DelayMS(200);
	CommBuf[49] = 0x40; UpdateLED(); DelayMS(200);
	CommBuf[49] = 0x80; UpdateLED(); DelayMS(200);
	CommBuf[49] = 0x08; UpdateLED(); DelayMS(200);
	CommBuf[49] = 0x04; UpdateLED(); DelayMS(200);
	CommBuf[49] = 0x02; UpdateLED(); DelayMS(200);
	CommBuf[49] = 0x20; UpdateLED(); 
	

	//Key
	TRISD = 0x0F;

	GIE = 1; PEIE = 1;

	i = 0;

	while(1)
	{
		Write_LCD_Comd4(0x0C);
		if (i == 0)
		{
			ShowMenu1();
			CommBuf[49] = 0x78; UpdateLED();
		}
		else if (i == 1)
		{
			ShowMenu1();
			CommBuf[49] = 0xA0; UpdateLED();
		}
		else if (i == 2)
		{
			ShowMenu2();
			CommBuf[49] = 0xB8; UpdateLED();
		}
		else
		{
			ShowMenu2();
			CommBuf[49] = 0x60; UpdateLED();
		}
		DelayMS(500);
		i++;
		if (i == 4) i = 0;
	}
}

//---------------------------- Interrupt handler

void interrupt IntHandler(void)
{
/*
// SPI Interrupt (it based on the communication protocol with Blackfin)
	if(SSPIF == 1)
	{
		SSPIE = 0;
		SSPIF = 0;
		if (SSPBUF == 0xAA)
		{
			SSPBUF = 0x00;
			spi_state = 0x01;	// Reset State, Next Command
		}
		else
		{
			if (spi_state == 0x01)
			{
				// command
				if ((SSPBUF & 0xC0) == 0x00) //Write to specified address
				{
					bufaddr = SSPBUF & 0x3F;
					spi_state = 0x02;	// Write Command
				}
				else if ((SSPBUF & 0xC0) == 0x40) //Read from specified address
				{
					bufaddr = SSPBUF & 0x3F;
					if (bufaddr == 0x04)
						SSPBUF = 0x33;	// ID
					else
						SSPBUF = CommBuf[bufaddr];
					bufaddr++;
					spi_state = 0x03;	// Read Command
				}
				else if ((SSPBUF & 0x80) == 0x80) //Special Command
				{
					switch(SSPBUF)
					{
						case 0x81: //Update LCD
							UpdateLCD_Int();
							break;
						case 0x82: //UpdateLED
							UpdateLED_Int();
							break;
						case 0x83: //Clear LCD
							Write_LCD_Comd_Int(0x01);
							for ( i = 16 ; i < 48; i++ )
								CommBuf[i] = 0x20;
							break;
						case 0x84: //Turn off all LEDs
							CommBuf[49] = 0xFF;
							UpdateLED_Int();
							break;
						case 0x85: //Ring a bell
							MakeAlarm_Int();
							break;
						case 0x86: //Reset Blackfin
							RB0 = 0;
							DelayMS_Int(100);
							RB0 = 1;
							break;
						default:
							break;
					}
					spi_state = 0x01;
				}
			}
			else
			{
				if (spi_state == 0x02)
				{
					CommBuf[bufaddr] = SSPBUF;
					bufaddr++;
				}
			}
		}
		SSPIE = 1;
		return;
	}
*/
}
